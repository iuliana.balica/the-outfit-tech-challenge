package ro.the_outfit_challenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import ro.the_outfit_challenge.resources.Product;
import ro.the_outfit_challenge.service.ExtractInfoService;

import java.util.List;

@SpringBootApplication
public class TheOutfitChallengeApplication {

    public static void main(String[] args) throws Exception {
        ApplicationContext applicationContext = SpringApplication.run(TheOutfitChallengeApplication.class, args);
        ExtractInfoService extractInfoService = applicationContext.getBean(ExtractInfoService.class);

        Product[] products = extractInfoService.getProducts();
        double average = extractInfoService.extractAverageOfKeptProducts(products);
        List<Long> top10Products = extractInfoService.top10ProductsWithHighestProbPurchase(products);
        List<String> top3Brands = extractInfoService.top3Brands(products);

        System.out.println("Average produse pastrate per comanda: " + average + "\n"
                + "Top 10 produse cu probabilitatea de cumparare cea mai mare: " + top10Products + "\n"
                + "Top 3 branduri cu cea mai mare probabilitate de cumparare: " + top3Brands);
    }
}
