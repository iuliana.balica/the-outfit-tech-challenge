package ro.the_outfit_challenge.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import ro.the_outfit_challenge.resources.Product;


@Service
@RequiredArgsConstructor
public class ApiIntegrationService {
    private final RestTemplate restTemplate;

    @Value("${api.endpoint}")
    private String apiEndpoint;

    @Value("${api.key}")
    private String apiKey;


    public Product[] getProducts() throws Exception {
        final HttpHeaders headers = prepareAuthHeaders();
        final HttpEntity<?> entity = new HttpEntity<>(headers);
        ResponseEntity<Product[]> response = null;
        try {
            response = this.restTemplate.exchange(apiEndpoint, HttpMethod.GET, entity, Product[].class);
        } catch (HttpClientErrorException.NotFound exception) {
            throw new Exception(exception.getMessage());
        }
        return response.getBody();
    }

    private HttpHeaders prepareAuthHeaders() {
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("API_KEY", apiKey);
        return headers;
    }
}
