package ro.the_outfit_challenge.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ro.the_outfit_challenge.resources.Product;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;
import static ro.the_outfit_challenge.resources.ProductStatus.VANDUT;

@Service
@RequiredArgsConstructor
public class ExtractInfoService {
    private final ApiIntegrationService apiIntegrationService;

    public Product[] getProducts() throws Exception {
        return apiIntegrationService.getProducts();
    }

    public double extractAverageOfKeptProducts(Product[] products) {
        Map<String, List<Product>> orders = Arrays.stream(products)
                .filter(p -> VANDUT.name().equals(p.getStatus()))
                .collect(groupingBy(Product::getOrderId));

        int noOrders = Arrays.stream(products)
                .collect(groupingBy(Product::getOrderId))
                .size();

        double sum = 0;
        for (Map.Entry<String, List<Product>> order : orders.entrySet()) {
            sum = sum + order.getValue().size();
        }
        return sum / noOrders;
    }

    public List<Long> top10ProductsWithHighestProbPurchase(Product[] products) {
        Map<Long, List<Product>> map = Arrays.stream(products).collect(groupingBy(Product::getProductId));
        Map<Long, Integer> probability = new HashMap<>();
        for (Map.Entry<Long, List<Product>> product : map.entrySet()) {
            int numberOfOrderedProducts = product.getValue().size();
            if (numberOfOrderedProducts > 5) {
                int numberOfSoldProducts = (int) product.getValue().stream()
                        .filter(o -> VANDUT.name().equals(o.getStatus()))
                        .count();
                probability.put(product.getKey(), numberOfSoldProducts / numberOfOrderedProducts);
            }
        }
        return probability.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .limit(10)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    public List<String> top3Brands(Product[] products) {
        Map<String, List<Product>> brands = Arrays.stream(products).collect(groupingBy(Product::getBrand));
        Map<String, Integer> probability = new HashMap<>();
        for (Map.Entry<String, List<Product>> product : brands.entrySet()) {
            int numberOfOrderedProducts = product.getValue().size();
            int numberOfSoldProducts = (int) product.getValue().stream()
                    .filter(o -> VANDUT.name().equals(o.getStatus()))
                    .count();
            probability.put(product.getKey(), numberOfSoldProducts / numberOfOrderedProducts);
        }

        return probability.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .limit(3).map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }
}
