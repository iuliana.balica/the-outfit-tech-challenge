package ro.the_outfit_challenge.resources;

import lombok.Data;

@Data
public class Product {
    private String orderId;
    private Long productId;
    private String size;
    private String status;
    private String brand;
}
